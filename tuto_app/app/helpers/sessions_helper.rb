module SessionsHelper
    def log_in
        @current_user ||= User.find_by(id: session[:user_id])
    end
    def log_out
        session.delete(:user_id)
    end
    def current_user
        @current_user ||= User.find_by(id: session[:user_id])
    end
    


end    