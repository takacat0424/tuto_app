
Rails.application.routes.draw do
    root "pages#top"

  get 'sessions/new'
  resources :users
  get 'users/new'
  get '/about', to: 'pages#about'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'
    

end
